const reveal = document.querySelectorAll(".reveal");
for (let each of Array.from(reveal)) {
    document.addEventListener('scroll', () => {
        const baseHeight = window.innerHeight;
        const top = each.getBoundingClientRect().top;
        if (top < baseHeight) {
            each.classList.add('reveal_active');
        };
    });
};