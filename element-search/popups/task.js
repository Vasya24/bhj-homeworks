openMain()

function openMain() {
    let main = document.getElementById("modal_main");
    setTimeout(() => {
        main.classList.add("modal_active");
    }, 500);
}

function closeModal() {
    let elements = document.getElementsByClassName("modal_active");
    Array.from(elements).forEach((element) => {
        element.classList.remove("modal_active");
    });
};

function success() {
    var element = document.getElementById("modal_success");
    setTimeout(() => {
        element.classList.add("modal_active");
    }, 300);
    closeModal();
}

const setOnClickEventForClass = (elementClassName, func) => {
    Array.from(document.getElementsByClassName(elementClassName)).forEach(element => {
        element.onclick = func;
    })
}

setOnClickEventForClass("modal__close_times", closeModal);
setOnClickEventForClass("btn_success", closeModal);
setOnClickEventForClass("show-success", success);