const images = document.getElementsByClassName("slider__item");
let currentImage = 0;

function sliderMove(direction) {
    if (direction === 1) {
        images[currentImage].className = 'slider__item';
        currentImage++;
        if (currentImage === images.length) { currentImage = 0 }
        images[currentImage].className += ' slider__item_active';
    }
    if (direction === 0) {
        images[currentImage].className = 'slider__item';
        currentImage--;
        if (currentImage < 0) { currentImage = (images.length - 1) }
        images[currentImage].className += ' slider__item_active';
    }
}


const arrowClickNext = document.getElementsByClassName("slider__arrow_next");
const arrowClickPrev = document.getElementsByClassName("slider__arrow_prev");
arrowClickNext[0].onclick = function() {
    sliderMove(1);
}


arrowClickPrev[0].onclick = function() {
    sliderMove(0);
}