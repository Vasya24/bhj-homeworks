const droplist = document.querySelector('.dropdown__list');
const btn = document.querySelector(".dropdown__value");
const listItem = document.querySelectorAll('.dropdown__link');

btn.addEventListener('click', function() {
    droplist.classList.toggle('dropdown__list_active');
})

for (let item of Array.from(listItem)) {
    item.onclick = () => {
        droplist.classList.toggle('dropdown__list_active');
        btn.textContent = item.textContent;
        return false;
    }
}