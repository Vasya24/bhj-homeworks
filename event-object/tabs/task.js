const tabs = Array.from(document.querySelectorAll(".tab"));
const tabContent = document.querySelectorAll(".tab__content");

function tabSelector(e) {
    for (let value in tabs) {
        tabs[value].className = "tab";
        tabContent[value].className = "tab__content";
    }
    e.target.className = "tab tab_active";
    tabContent[tabs.indexOf(event.target)].className = "tab__content tab__content_active";
};

for (let tab in tabs) {
    tabs[tab].addEventListener('click', tabSelector);
};