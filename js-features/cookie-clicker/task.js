const cookie = document.getElementById('cookie');
const counter = document.getElementById('clicker__counter');
const speed = document.getElementById('speed__counter');

sizeChange = () => {
    cookie.width = 400;
    counter.textContent++;
    setTimeout(() => cookie.width = 200, 100);
    speedCount();
}

let time = [];

speedCount = () => {
    if (cookie.onclick) {
        time.push(Date.now());
        if (time.length === 2) {
            speed.textContent = 1 / ((time[1] - time[0]) / 1000);
        } else if (time.length > 2) {
            time.splice(0, 1);
        }
    }
}

cookie.onclick = sizeChange;